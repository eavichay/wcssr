const path = require('path');
// const HtmlWebpackPlugin = require('html-webpack-plugin')
// const CleanWebpackPlugin = require('clean-webpack-plugin')
// const CopyWebpackPlugin = require('copy-webpack-plugin')

const distFolder = path.resolve(__dirname, 'dist')

const isProduction = process.env.NODE_ENV === 'production'

module.exports = {
    entry: {es: './demo-app/src/app.js'},
    output: {
        path: distFolder,
        filename: 'bundle.js'
    },

    // if NODE_ENV is set to "production" webpack will also minify the files
    mode: isProduction ? 'production' : 'development',

    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 9000
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader'
            },
            {
                test: /\.html?$/,
                loader: 'html-loader',
                options: {
                    preprocessor: (content, loaderContext) => {
                        return content
                            .split('<%-').join('{{')
                            .split('<%=').join('{{')
                            .split('<%').join('{{')
                            .split('%>').join('}}');
                    },
                }
            },
        ]
    },

    // no source maps for production
    devtool: isProduction ? undefined : 'source-map',

    target: 'web',
    stats: 'errors-only',

    // plugins: [
    //     new CleanWebpackPlugin([distFolder]),
    //     new CopyWebpackPlugin([
    //         {
    //             from: 'src/assets',
    //             to: distFolder + '/assets'
    //         },
    //         {
    //             from: 'manifest.json',
    //             to: distFolder
    //         },
    //         {
    //             from: 'sw.js',
    //             to: distFolder
    //         },
    //         {
    //             from: 'icons',
    //             to: distFolder + '/icons'
    //         }
    //     ]),
    //     new HtmlWebpackPlugin({
    //         template: './index.html'
    //     })
    // ]
};
