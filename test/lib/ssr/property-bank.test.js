import { __SSR_PROPERTY_BANK as unit } from '../../../lib/ssr/property-bank';
const assert = require('assert');

describe('Property Bank', () => {
  beforeEach(() => unit.__CLEAR());
  afterEach(() => unit.__CLEAR());

  it('Should set and get a property', () => {
    const stub = {
      ssr: 'Awesome'
    };
    unit.setProperty('ns', 'a', 1);
    unit.setProperty('ns', 'stub', stub);

    assert.strictEqual(unit.getProperty('ns', 'a'), 1);
    assert.strictEqual(unit.getProperty('ns', 'stub'), stub);
  });

  it('Should apply properties on target and clear', () => {
    const target = {};
    const stub = {
      ssr: 'Awesome'
    };
    const someValue = 1;
    unit.setProperty('1time', 'stub', stub);
    unit.setProperty('1time', 'someValue', someValue);
    unit.applyOnTarget('1time', target);

    assert.strictEqual(target.someValue, someValue);
    assert.strictEqual(target.stub, stub);
    assert.strictEqual(unit.getProperty('1time', 'someValue'), undefined);
    assert.strictEqual(unit.getProperty('1time', 'stub'), undefined);
  });

  it('Should clear', () => {
    unit.setProperty('ns', 'test', 123);
    unit.__CLEAR();
    assert.strictEqual(unit.getProperty('ns', 'test'), undefined);
  });

});
