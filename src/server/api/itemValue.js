const service = require('../../shared/dataService');

module.exports = (app) => {
  app.get('/api/item/:id', (req, res, next) => {
    const result = service.getItem(req.params.id);
    res.type('text');
    res.send(result);
  })
}