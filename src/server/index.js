const path = require('path');
const fs = require('fs');
const express = require('express');
const ejs = require('ejs');

const itemInfoEndpoint = require('./api/itemValue');

const INDEX_HTML = path.resolve(__dirname, 'public/index.ejs');
const ITEM_INFO = path.resolve(__dirname, 'public/item-info.ejs');

const service = require('../shared/dataService');


const app = express();

app.get('/', async (req, res, next) => {
  const result = await ejs.renderFile(INDEX_HTML, {
    pageContent: await ejs.renderFile(ITEM_INFO, {
      itemvalue: service.getItem(2)
    })
  });
  console.log(result);
  res.type('html');
  res.send(result);
});

itemInfoEndpoint(app);

app.use(express.static('src/server/public'));
app.use(express.static('dist'));

app.listen(8000);