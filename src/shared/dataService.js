const cache = ['foo', 'bar', 'baz', 'qux', 'ninja', 'pach'];

module.exports = {
  getItem: (id) => {
    return cache[id] || 'Unknown Item';
  }
};