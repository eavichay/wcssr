import { Framework } from './framework.js';

const template = require('../server/public/item-info.template.html');

class ItemInfo extends Framework {

  get useShadow() {
    return true;
  }

  get template() {
    return template;
  }

  static get observedAttributes() {
    return ['item-id'];
  }

  attributeChangedCallback(attr, oldvalue, newvalue) {
    fetch(`/api/item/${newvalue}`).then(r => r.text()).then(value => this.itemvalue = value).then(() => this.render());
  }
}

customElements.define('item-info', ItemInfo);