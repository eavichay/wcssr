import { Slim } from 'slim-js';

export class Framework extends Slim {
  constructor() {
    super();
  }

  render(tpl) {
    if (this.hasAttribute('ssr')) {
      const source = this.querySelector('ssr-shadow-root');
      if (source) {
        source.remove();
        super.render(source.innerHTML);
        this.removeAttribute('ssr');
        this.setAttribute('ssr-hydrated', '');
      }
    } else {
      super.render(tpl);
    }
  }
}