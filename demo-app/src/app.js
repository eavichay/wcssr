import { Slim } from 'slim-js';
import { tag, template, useShadow } from 'slim-js/Decorators';


class MasterBlaster extends Slim {
  _hydrated = false;

  render(tpl) {
    if (!this._hydrated) {
      const propBank = this.getAttribute('ssr');
      if (propBank) {
        const template = this.querySelector('shadow-root');
        __SSR_PROPERTY_BANK.applyOnTarget(propBank, this);
        this._hydrated = true;
        this.setAttribute('ssr-hydrated', '');
        this.removeAttribute('ssr');
        if (template) {
          template.remove();
          super.render(template.innerHTML);
        } else {
          super.render(tpl);
        }
      }
    }
  }

  constructor() {
    super();
  }
}

@useShadow(true)
@template(require('./x-com.tpl.html'))
export class XCOM extends MasterBlaster {
  constructor() {
    super();
  }
}

setTimeout(() => {
  Slim.tag('x-com', XCOM);
}, 5000);