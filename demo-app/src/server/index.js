import path from 'path';
import { readFileSync } from 'fs';
import mustache from 'mustache';
import { initialize } from '../../../lib/ssr/server.js';

const xcom = () => readFileSync(path.resolve(path.dirname(''), 'demo-app', 'src', 'x-com.tpl.html')).toString();


const Component = (tag, data, state, srcPath = 'src') => {
  const filepath = path.resolve(path.dirname(''), 'demo-app', `${(srcPath ? srcPath + '/' : '') + tag}.tpl.html`);
  const hash = new Buffer(Date.now() + '' + Math.random()).toString('base64');
  state[hash] = data;
  const html = /*html*/`
    <${tag} ssr="${hash}">
      ${mustache.render(
        readFileSync(filepath).toString(),
        {
          ...state, 
          ...data,
        }
      )}
    </${tag}>
  `;
  return html;
};

const appState = {};

const testItem = {
  name: 'Avichay',
  age: 42
};

const app = initialize({
  root: path.resolve(path.dirname(''), 'demo-app', 'src'),
  staticPaths: [path.resolve(path.dirname(''), 'dist')],
  contentProvider: async (req) => {
    return {
      html: /*html*/`
        <h1>Demo Web components SSR</h1>
        <hr/>
        ${Component('x-com', { item: testItem }, appState)}
        `,
      state: appState,
    };
  }
});