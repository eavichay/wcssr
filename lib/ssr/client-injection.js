import { render } from 'mustache';

/**
 * @typedef PartialRenderOptions
 * @property {boolean} [shadow]
 * @property {string} template
 * @property {string} hash
 * @property {object} data
 */


/**
 * @param {PartialRenderOptions} opts 
 * @returns {string}
 */

export const renderPartial = (opts) => {
  const result = render(opts.template, opts.data);
  return result;
}