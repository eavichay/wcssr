import propertyBank from './property-bank';
import mustache from 'mustache/mustache.mjs';

if (typeof window !== 'undefined') {
  window.__SSR_PROPERTY_BANK = propertyBank;
  window.__SSR_TPARSER = mustache;
}