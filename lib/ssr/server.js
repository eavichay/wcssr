import express from 'express';
import mustache from 'mustache';
import fs from 'fs';
import path from 'path';

const isProduction = process.NODE_ENV === 'production';

/**
 * @typedef PageContent
 * @property {string} html
 * @property {object} state
 */

/**
 * @typedef {(req: import('express').Request) => Promise<PageContent>} ContentProvider
 */


/**
 * @typedef SSRServerOptions
 * @property {string} [index] defaults to './index.tpl.html'
 * @property {string} [bundle] defaults to './app.js'
 * @property {string[]} [staticPaths] additional static paths, defaults to ['./assets', './public']
 * @property {ContentProvider} contentProvider
 * @property {string} root root path
 * @property {number} [port] defaults to 80 for production, 8181 for development
 */

/** @type SSRServerOptions */
const defaultOptions = {
  bundle: 'app.js',
  index: 'index.tpl.html',
  staticPaths: ['./assets', './public'],
  contentProvider: async (req) => 'Hello, wolrd',
  port: isProduction ? 80 : 8181,
}

/** @type Express */
var app;


const fileCache = {};
const getFileContent = async (path) => {
  return new Promise((resolve, reject) => {
    if (fileCache[path]) {
      resolve(fileCache[path]);
    }
    fs.readFile(path, {}, (err, data) => {
      if (err) {
        reject(err);
      } else {
        if (isProduction) {
          resolve(fileCache[path] = data.toString());
        } else {
          resolve(data.toString());
        }
      }
    });
  });
}


/**
 * @param {SSRServerOptions} options 
 * @returns {Express}
 */
export async function initialize(options) {
  if (app) {
    throw new Error('Cannot initialize twice');
  }

  const initOptions = Object.assign({}, defaultOptions, options);

  const injections = await getFileContent(path.resolve(path.dirname(''), 'lib', 'ssr', 'property-bank.js'));

  app = express();

  [initOptions.root, ...initOptions.staticPaths].forEach((path) => {
    console.info('Static path', path);
    app.use(express.static(path));
  });

  app.get('/ssr/client-bootstrap.js', (req, res) => {
    res.type('text/javascript');
    res.send(injections);
  });

  app.get('/', async (req, res) => {
    try {
      const pageContent = await initOptions.contentProvider(req);
      const clientState = Object.keys(pageContent.state).reduce((script, key) => {
        const data = pageContent.state[key];
        if (typeof data === 'object') {
          Object.keys(data).forEach((prop) => {
            const value = JSON.stringify(data[prop]);
            script += `__SSR_PROPERTY_BANK.setProperty('${key}', '${prop}', ${value});`;
          });
        }
        return script;
      }, '');
      const result = mustache.render(
        (await getFileContent(path.resolve(initOptions.root, initOptions.index))).replace('{{ APP_CONTENT }}', pageContent.html).replace('{{ STATE }}', `<script>${clientState}</script>`)
        , {
          ...pageContent.state
      });
      res.type('html');
      res.send(result);
    } catch (err) {
      console.error(err);
      res.send(err);
    }
  });

  app.listen(initOptions.port);
  return app;
}