(function (ROOT) {
  
  const bank = {};

  if (ROOT) {
    ROOT.__SSR_PROPERTY_BANK = Object.freeze({
      setProperty: (namespace, propertyName, value) => {
        if (!Object.getOwnPropertyDescriptor(bank, namespace)) {
          bank[namespace] = {};
        }
        bank[namespace][propertyName] = value;
      },

      getProperty: (namespace, propertyName) => {
        return (bank[namespace] || {})[propertyName];
      },

      applyOnTarget: (namespace, target) => {
        Object.assign(target, (bank[namespace] || {}));
        delete bank[namespace];
      },

      __CLEAR: () => {
        Object.keys(bank).forEach((key) => delete bank[key]);
      },

    });
  }
})(typeof window === 'undefined' ? exports : window);
